# **NEXT LOGICAL STEP(S)**

    Add the waveform scrollers
-----
    Play some sound
-----
    Access ASIO device info

## Beta Release _development checklist_
_delete once all &ldquo;Beta Blocker&rdquo; issues are resolved!_


**[SAMPLES](https://learn.microsoft.com/en-us/windows/win32/learnwin32/learn-to-program-for-windows--sample-code)**

**[CONTROL SPY](https://learn.microsoft.com/en-us/windows/win32/controls/control-spy)**

_[other tools?](https://www.oreilly.com/library/view/windows-developer-power/0596527543/)_


- [x] start with (revert to) example from https://learn.microsoft.com/en-us/windows/win32/learnwin32/your-first-windows-program
- [x] define _pixels per second_ for the live waveform, calculate total pixel width of waveform
- [x] we can probably set up each UI element in its own source file
- [ ] set up some object (window/control) placeholders:
    - [ ] waveform display
    - [ ] live waveform display
    - [x] progress text
    - [ ] duration text
    - [x] time text (and what about when not recording?)
    - [ ] name text
    - [x] other info text
    - [x] buttons
    - [x] progress bar
    - [ ] icons
    - [ ] status, to show processing state or import/export state
- [ ] put all X/Y/H/W into headers or calculations
- [ ] graphics e.g. waveform ([related issue](https://gitlab.com/PennRobotics/M4QR/-/issues/8)) could be drawn with D2D: https://learn.microsoft.com/en-us/windows/win32/learnwin32/module-2--using-com-in-your-windows-program + https://learn.microsoft.com/en-us/windows/win32/learnwin32/module-3---windows-graphics &rarr; https://learn.microsoft.com/en-us/windows/win32/learnwin32/your-first-direct2d-program
- [ ] update CMake to link correctly to D2D
- [ ] What is going to happen on a totally fresh dev machine? Will _vcpkg_ help? ( https://www.youtube.com/watch?v=0h1lC3QHLHU )
- [ ] save dialog ([related issue](https://gitlab.com/PennRobotics/M4QR/-/issues/6)) probably similar to [open dialog](https://learn.microsoft.com/en-us/windows/win32/learnwin32/example--the-open-dialog-box)
- [ ] error handling? https://gitlab.com/PennRobotics/M4QR/-/issues/37 https://learn.microsoft.com/en-us/windows/win32/learnwin32/error-handling-in-com
- [ ] [inputs](https://gitlab.com/PennRobotics/M4QR/-/issues/38): https://learn.microsoft.com/en-us/windows/win32/learnwin32/module-4--user-input
- [ ] save individual channels plus combined channel (these could be toggleable)

* _Contingency plan: if plain C/C++ is too much of a pain, use XAML and C++ via Petzold: https://resources.oreilly.com/examples/9780735671768-files ._

### References

- https://waveshop.sourceforge.net/index.html
- https://learn.microsoft.com/en-us/windows/win32/desktop-programming
- https://microsoft.fandom.com/wiki/Windows_API#External_links
- https://www.os2museum.com/wp/windows-history/windows-library/
- https://github.com/Microsoft/windows-dev-box-setup-scripts




-----
<!-- DELETE EVERYTHING ABOVE THIS LINE -->



# M4 Quick Recorder

![Screenshot of a mostly empty, tall, rectangular window with a record button at the bottom](screenshot-main-window.png "Main Window Screenshot"){height=320px}

The philosophy of this program is to simplify recording via the MOTU M4 USB audio interface (ASIO).

Most DAW software is incredibly powerful, yet most present many options to the musician that slow down a simple jam session or rehearsal. There&rsquo;s a whole process of selecting input channels, connecting them to the output (optionally through plugins), moving gain sliders for the input and selected output, arming a track, recording a track, moving the playback cursor, possibly deleting the contents of the track, and so on. Once the software is then running and recording, the CPU load is often high enough that the fans start spinning audibly, which adds a little bit of noise to your recording.

Compare this to an Android app used for sound recording. Often, there&rsquo;s a big, red, round _Record_ button, a navigation slider for changing the playback location, and a few other buttons in the corner for setting the default filename or looking up an old recording to hear or share. With the expectation that a phone is able to record while staying cool, a PC program with the same functionality should also not be incredibly demanding of the computer&rsquo;s resources.

That is all to say: PC recording software can do better, and I hope this project can advance toward this goal.


## Installation

At some point, I&rsquo;ll upload releases in executable format. These should never require installation (at least until I have to figure out persistent settings or library dependencies or something else).


## Build

This is somewhat rare territory of an open-source Visual Studio project that compiles in C++ using CMake. My background is compiling for AVR and Arm microcontrollers in C from Linux using gcc commands and occasionally Makefiles, so I am often underwater when it comes to CMake, although I have heard there is a great book.

If you are considering building, you are probably already a step ahead of me here, knowledge- or experience-wise.


## Support

At this point, I am not looking for this project to blow up in popularity and wildly expand its feature set. In fact, I&rsquo;m aiming for quite the opposite. I just want to turn on my audio interface, plug in a mic, turn on this program, press Record, and start playing! There are dozens of very capable applications for providing metronome ticks, effects routing, plugin support, buffer adjustments, multi-track playback during multi-track recording, MIDI, and so much more.

The next Shania Twain hit will not be recorded with this program. It is a pet project for quick, small, low fidelity, mostly unmodified recordings that I can analyze later, archive for gauging progress, or send to friends and family.

With that in mind: if you want something cool implemented, you have a similar setup to mine, and you are relatively **un**comfortable coding, open an Issue. I am notoriously bad at working on projects and have terrible time management of my already thin availability, but outside pressure and cool ideas are usually good motivators.


## Roadmap

See Milestones for now.


## Contributing

# TODO: add a source tree here

Pull request! Try to stick to the philosophy of oversimplifying this application. If you know you are adding major functionality or expanding the features of this program in a way that will slow down the initial configuration or workflow, maybe jump straight to forking. (In fact, forking is probably best, and then when I have time and like functionality of a specific fork, I would probably add that to my own repo&mdash;something like a _reverse pull request_.)

### Two Exceptions

1. Testing
2. Cross-platform

If you want to make this thing cross-platform _in a way that does not vastly obscure the Windows source_, please be my guest! What I am **not** looking for is a ton of `#define`s or something quasi-commercial like Qt. However, if you are clever enough to separate out the UI and other functionality and then make a few source files for another OS, that would be pretty cool, and I would definitely support that. (For the longest time, I used Linux exclusively and know the struggle of finding great apps.)

Testing is another area where I simply do not have enough experience (outside of Python). I am not sure there is much to be tested here, as I am really looking to merge three very mature or commercial codebased (Windows SDK, ASIO, an mp3 encoder) with very little glue.

I doubt there will be much time I would personally devote to testing or cross-platform compatibility unless somebody else gets the ball rolling.


## Authors and acknowledgment

# TODO: show your appreciation to those who have contributed to the project.


## License

# TODO
(This probably depends on the linked dependencies and tools used, which are probably going to be ASIO, the Windows SDK with a community version of Visual Studio, CMake, and LAME.)


## Project status

_I have run out of energy or time for my project._

Honestly, development will probably happen at a snail&rsquo;s pace, because I very often hit a wall during development (e.g. trying to put a built-in icon on a Windows button in C++) and then need to trudge through until I accidentally find an answer, eventually decide to drop the feature and move in a different direction, or lose motivation and stop working on the project for a while.
