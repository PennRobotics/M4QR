#pragma once

static const wchar_t* szSave = L"&Save";
static const wchar_t* szDiscard = L"&Discard Changes";

BOOL DialogSettings(HWND, UINT, WPARAM, LPARAM);
