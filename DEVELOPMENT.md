```mermaid
graph BT;
  ClickableIcon --> M4QR
  DialogAbout --> M4QR
  DialogSettings --> M4QR
  Sound --> M4QR
  TrackScroller --> M4QR
  helper --> ClickableIcon
  helper --> DialogAbout
  helper --> DialogSettings
  helper --> Sound
  helper --> TrackScroller
```
