#pragma comment(linker,"/manifestdependency:\"type='win32' name = 'Microsoft.Windows.Common-Controls' version = '6.0.0.0' processorArchitecture = '*' publicKeyToken = '6595b64144ccf1df' language = '*'\"")

#include <SDKDDKVer.h>
#include <windows.h>

//#include <stdlib.h>
//#include <malloc.h>
//#include <memory.h>
//#include <tchar.h>

/// #include "ClickableIcon.h"
#include "DialogAbout.h"
#include "DialogSettings.h"
/// #include "Sound.h"
/// #include "TrackScroller.h"

#include "M4QR.h"

HINSTANCE hInst;
HWND hWndMain;

ATOM              RegisterM4QRClass(HINSTANCE);
BOOL              InitInstance(HINSTANCE, int);
LRESULT CALLBACK  WndProc(HWND, UINT, WPARAM, LPARAM);


int APIENTRY wWinMain(_In_     HINSTANCE hInstance,
                      _In_opt_ HINSTANCE hPrevInstance,
                      _In_     LPWSTR    lpCmdLine,
                      _In_     int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

    if ( !RegisterM4QRClass(hInstance) ) {
        MessageBox(NULL, L"M4QR failed to register its main class", szTitle, NULL);
    }

    if ( !InitInstance(hInstance, nCmdShow) )  { return FALSE; }

    MSG msg;
    while ( GetMessage(&msg, nullptr, 0, 0) ) {
        TranslateMessage(&msg);
        DispatchMessage(&msg);
    }

    return (int)msg.wParam;
}


ATOM RegisterM4QRClass(HINSTANCE hInstance)
{
    WNDCLASSEXW wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);

    wcex.style         = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc   = WndProc;
    wcex.cbClsExtra    = 0;
    wcex.cbWndExtra    = 0;
    wcex.hInstance     = hInstance;
    wcex.hIcon         = nullptr;
    wcex.hCursor       = LoadCursor(nullptr, IDC_ARROW);
    wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
    wcex.lpszMenuName  = nullptr;
    wcex.lpszClassName = szWindowClass;
    wcex.hIconSm       = nullptr;

    return RegisterClassEx(&wcex);
}


// Saves instance handle in global var, creates/shows main window
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
    hInst = hInstance;  // Store instance handle globally
    HWND hWnd = CreateWindow(szWindowClass, szTitle,
        WS_OVERLAPPEDWINDOW,
        CW_USEDEFAULT, CW_USEDEFAULT, MAIN_WIDTH + 2*SM_CXDLGFRAME, MAIN_HEIGHT + SM_CYSIZE,
        nullptr, nullptr, hInstance, nullptr);
    if (!hWnd)  { return FALSE; }
    hWndMain = hWnd;

    ShowWindow(hWnd, nCmdShow);
    UpdateWindow(hWnd);  // sends WM_PAINT

    return TRUE;
}


/**
 *  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
 *
 *  PURPOSE: Processes messages for the main window.
 *
 *  WM_CREATE  - create the application
 *  WM_COMMAND - process the application menu
 *  WM_PAINT   - paint the main window
 *  WM_DESTROY - post a quit message and return
 */
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch (message) {
        case WM_CREATE:
            {
                // ----------
                // Tracks
                // ----------
                /* TODO-hi: Create scrollers for each channel */
                
                // ----------
                // Buttons
                // ----------
                HWND hBtnRecord = CreateWindow(L"BUTTON", L"REC",
                    WS_CHILD | WS_VISIBLE | BS_DEFPUSHBUTTON,
                    156, 716, 48, 48,
                    hWnd, NULL, hInst, NULL);

                CreateWindow(L"BUTTON", L"1",
                    WS_CHILD | WS_VISIBLE,
                    10, 724, 32, 32,
                    hWnd, NULL, hInst, NULL);
                CreateWindow(L"BUTTON", L"2",
                    WS_CHILD | WS_VISIBLE,
                    42, 724, 32, 32,
                    hWnd, NULL, hInst, NULL);
                CreateWindow(L"BUTTON", L"3",
                    WS_CHILD | WS_VISIBLE,
                    74, 724, 32, 32,
                    hWnd, NULL, hInst, NULL);
                CreateWindow(L"BUTTON", L"4",
                    WS_CHILD | WS_VISIBLE,
                    106, 724, 32, 32,
                    hWnd, NULL, hInst, NULL);

                CreateWindow(L"BUTTON", L"Set",
                    WS_CHILD | WS_VISIBLE,
                    238, 724, 32, 32,
                    hWnd, NULL, hInst, NULL);
                CreateWindow(L"BUTTON", L"Lib",
                    WS_CHILD | WS_VISIBLE,
                    302, 724, 32, 32,
                    hWnd, NULL, hInst, NULL);

                // ----------
                // Scrollbar
                // ----------
                HWND hScrollbar = CreateWindow(L"SCROLLBAR", NULL,
                    WS_CHILD | WS_VISIBLE | WS_HSCROLL | SBS_HORZ,
                    10, 652, 340, 18,
                    hWnd, NULL, hInst, NULL);
                // TODO: provide a handle and message handler

                // ----------
                // Fixed Text Labels
                // ----------
                const wchar_t* szStaticTimestamp = L"00:00";
                HWND hStaticTimestamp = CreateWindow(L"STATIC", NULL,
                    WS_CHILD | WS_VISIBLE | SS_CENTER | SS_CENTERIMAGE,
                    120, 580, 120, 40,
                    hWnd, NULL, hInst, NULL);
                if (!hStaticTimestamp & DEBUG) { MessageBox(NULL, L"hStaticTimestamp = 0", L"Err", MB_OK | MB_ICONERROR); }
                if (hStaticTimestamp) {
                    LOGFONT lf;  // TODO: move to WM_SETFONT?
                    ZeroMemory(&lf, sizeof(lf));
                    lf.lfCharSet = DEFAULT_CHARSET;
                    lf.lfHeight = -40;
                    HFONT hFont = CreateFontIndirect(&lf);
                    SendMessage(hStaticTimestamp, WM_SETFONT, (WPARAM)hFont, TRUE);

                    SendMessage(hStaticTimestamp, WM_SETTEXT, (WPARAM)NULL, (LPARAM)szStaticTimestamp);
                }

                const wchar_t* szStaticRecording = L"Recording...";
                HWND hStaticRecording = CreateWindow(L"STATIC", NULL,
                    SS_CENTER | SS_CENTERIMAGE | WS_CHILD | WS_VISIBLE,
                    120, 620, 120, 18,
                    hWnd, NULL, hInst, NULL);
                if (!hStaticRecording & DEBUG) { MessageBox(NULL, L"hStaticRecording = 0", L"Err", MB_OK | MB_ICONERROR); }
                if (hStaticRecording) {
                    SendMessage(hStaticRecording, WM_SETTEXT, (WPARAM)NULL, (LPARAM)szStaticRecording);
                }

                const wchar_t* szStaticFileDetails = L"0 MB, MP3, 48kHz";
                HWND hStaticFileDetails = CreateWindow(L"STATIC", NULL,
                    SS_CENTER | SS_CENTERIMAGE | WS_CHILD | WS_VISIBLE,
                    120, 638, 120, 12,
                    hWnd, NULL, hInst, NULL);
                if (!hStaticFileDetails & DEBUG) { MessageBox(NULL, L"hStaticFileDetails = 0", L"Err", MB_OK | MB_ICONERROR); }
                if (hStaticFileDetails) {
                    LOGFONT lf;  // TODO: move to WM_SETFONT?
                    ZeroMemory(&lf, sizeof(lf));
                    lf.lfCharSet = DEFAULT_CHARSET;
                    lf.lfHeight = -12;
                    HFONT hFont = CreateFontIndirect(&lf);
                    SendMessage(hStaticFileDetails, WM_SETFONT, (WPARAM)hFont, TRUE);

                    SendMessage(hStaticFileDetails, WM_SETTEXT, (WPARAM)NULL, (LPARAM)szStaticFileDetails);
                }

                /* TODO: Create info text labels (some dynamic e.g. timestamp) */

                /* TODO: Create about dialog */

                /* TODO: Create save dialog */

                /* TODO-lo: Create status bar */
            }
            break;

        case WM_CTLCOLORSTATIC:
            SetTextColor((HDC)wParam, RGB(0, 0, 0));
            SetBkMode((HDC)wParam, TRANSPARENT);
            return (LRESULT)GetStockObject(HOLLOW_BRUSH);

        case WM_COMMAND:
            switch ( LOWORD(wParam) ) {
                case IDM_EXIT:
                    DestroyWindow(hWnd);
                    break;

                default:
                    return DefWindowProc(hWnd, message, wParam, lParam);
            }

            break;

        case WM_PAINT:
            /*{
                PAINTSTRUCT ps;
                HDC hdc = BeginPaint(hWnd, &ps);
                
                EndPaint(hWnd, &ps);
            }//*/
            break;

        case WM_DESTROY:
            PostQuitMessage(0);
            break;

        default:
            return DefWindowProc(hWnd, message, wParam, lParam);
    }

    return 0;
}
