#pragma once

const wchar_t* szTitle = L"M4 Quick Recorder";
const wchar_t* szWindowClass = L"M4QR";

#define MAIN_HEIGHT  800
#define MAIN_WIDTH   360

#define RECORD_PX_PER_SEC     20
#define PLAYBACK_SCROLLER_PX  480

//#define IDR_ACCELS       100
//#define IDR_MENU         101

//#define IDD_M4QR         200
//#define IDD_ABOUT        201
//#define IDD_SETTINGS     202

//#define IDC_CURSOR       300
#ifndef IDC_STATIC
#define IDC_STATIC       -1
#endif

//#define IDI_ICON         400

//#define IDB_BITMAP       500

//#define IDS_APP_TITLE    600

#define IDM_EXIT         700
//#define IDM_ABOUT        701

//#define ID_MFC_CMD       800




