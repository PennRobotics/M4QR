#include "helper.h"
#include "DialogAbout.h"


BOOL DialogAbout(HWND h, UINT msg, WPARAM wp, LPARAM lp)
{
    UNREFERENCED_PARAMETER(lp);

    switch (msg) {
        case WM_INITDIALOG:
            return TRUE;
        
        case WM_COMMAND:
            if (wp == IDOK || wp == IDCANCEL) {  /* TODO: get rid of cancel */
                EndDialog(h, wp);
                return TRUE;
            }
            break;

        case WM_CLOSE:
            EndDialog(h, IDCANCEL);  /* TODO: fallthrough */
            break;
    }

    return FALSE;
}
