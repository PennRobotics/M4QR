#include "helper.h"
#include "DialogSettings.h"


BOOL DialogSettings(HWND h, UINT msg, WPARAM wp, LPARAM lp)
{
    UNREFERENCED_PARAMETER(lp);

    switch (msg) {
        case WM_INITDIALOG:
            {
                HWND hOwner;
                RECT rc, rcOwner;

                hOwner = GetParent(h) ? GetParent(h) : GetDesktopWindow();

                GetWindowRect(hOwner, &rcOwner);
                CopyRect(&rc, &rcOwner);
                OffsetRect(&rc, 20, 20);

                SetWindowPos(h,
                            HWND_TOP,
                            rc.left,
                            rc.top,
                            0, 0,  // TODO: size?
                            SWP_NOSIZE /* TODO: ?? */);

                /*if (GetDlgCtrlID((HWND)wp) != ID_ITEMNAME) {
                    SetFocus( GetDlgItem(h, ID_ITEMNAME) );
                    return FALSE;
                }//*/
                return TRUE;
            }

        case WM_COMMAND:
            /* TODO */
            if (wp == IDOK || wp == IDCANCEL) {
                EndDialog(h, TRUE);
                return TRUE;
            }
            break;
    }

    return FALSE;
}
